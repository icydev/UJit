#include "Program.h"
#include <cassert>

namespace UJit {
	Function::Function(Type returnType) : returnType(returnType) {}

	const u8* Function::CompileTo(MemAllocator &memory) {
		u8* target = memory.Reserve((u32)assembler.Size());
		assembler.Compile(target);
		return target;
	}

	void Function::Call(const u8* address) {
		assembler.Call(address);
	}

	void Function::Ldc(intptr_t val) {
		eval.push(val);
	}

	void Function::Ret() {
		if (returnType == Type::I32) {
			assembler.Mov(eax, (i32)eval.top());
			eval.pop();
		} else if (returnType != Type::Void) {
			assert(false);
		}
		assembler.Ret();
	}
}
