#include "Memory.h"
#include <Windows.h>
#include <cassert>

#ifdef _WIN32
#define WORD_SIZE sizeof(WORD)
#endif

namespace MemPagePrivate {
	u32 PageSize;

	struct Init {
		Init() {
			SYSTEM_INFO si;
			GetSystemInfo(&si);
			PageSize = si.dwPageSize;
		}
	} init;

	int RoundUp(int numToRound, int multiple) {
		assert(multiple);
		return ((numToRound + multiple - 1) / multiple) * multiple;
	}
}

#define P MemPagePrivate

namespace UJit {
	MemPage::MemPage(PermissionFlags perms) {
		DWORD pagePerms = 0;
		if (perms == Permissions::ReadWriteExec)
			pagePerms = PAGE_EXECUTE_READWRITE;

		ptr = (u8*)VirtualAlloc(NULL, P::PageSize, MEM_COMMIT, pagePerms);
	}

	MemPage::MemPage(MemPage&& old) : ptr(old.ptr) {
		old.ptr = nullptr;
	}

	MemPage::~MemPage() {
		if (ptr) {
			VirtualFree(ptr, 0, MEM_RELEASE);
		}
	}

	u8* MemPage::Ptr() {
		return ptr;
	}

	MemAllocator::MemBlock::MemBlock(u8* ptr, u32 size) : Ptr(ptr), Size(size) {}

	MemAllocator::MemAllocator(PermissionFlags perms) : perms(perms) {}

	u8* MemAllocator::Reserve(u32 size) {
		size = P::RoundUp(size, WORD_SIZE);

		if (size == P::PageSize) {
			pages.push_back(MemPage(Permissions::ReadWriteExec));
			return pages.back().Ptr();
		}

		for (auto iter = free.rbegin(); iter != free.rend(); ++iter) {
			if (iter->Size >= size) {
				if (iter->Size > size) {
					addBlock(iter->Ptr + size, iter->Size - size);
				}
				u8* ret = iter->Ptr;
				free.erase(iter.base());
				return ret;
			}
		}

		pages.push_back(MemPage(Permissions::ReadWriteExec));
		addBlock(pages.back().Ptr() + size, P::PageSize - size);
		return pages.back().Ptr();
	}

	void MemAllocator::addBlock(u8* ptr, u32 size) {
		for (auto iter = free.rbegin(); iter != free.rend(); ++iter) {
			if (iter->Size >= size) {
				free.insert(iter.base() + 1, MemBlock(ptr, size));
			}
			return;
		}

		free.insert(free.begin(), MemBlock(ptr, size));
	}

	PermissionFlags operator|(Permissions lhs, Permissions rhs) { return PermissionFlags(lhs) | rhs; }
	PermissionFlags operator&(Permissions lhs, Permissions rhs) { return PermissionFlags(lhs) & rhs; }
}
