#include "Jitter.h"
#include "util/File.h"
#include <functional>
#include <cassert>

using namespace std;
using namespace util;

namespace UJit {
	namespace JitterPrivate {
		function<void(FileStream&, Function&, const vector<const u8*>&)> ParseFuncs[256];

		void Call(FileStream& in, Function& func, const vector<const u8*>& compiledFuncs) {
			u32 funci = in.Read<u32>();
			if (compiledFuncs[funci]) {
				func.Call(compiledFuncs[funci]);
			} else {
				assert(false);
			}
		}
		void LdcS(FileStream& in, Function& func, const vector<const u8*>&) { func.Ldc(in.Read<u8>()); }
		void Ret(FileStream& in, Function& func, const vector<const u8*>&) { func.Ret(); }

		struct Init {
			Init() {
				ParseFuncs[0x1F] = LdcS;
				ParseFuncs[0x28] = Call;
				ParseFuncs[0x2A] = Ret;
			}
		} init;
	}

#define P JitterPrivate

	Jitter::Jitter() : alloc(Permissions::ReadWriteExec) {}

	void Jitter::Compile(const std::string &filename) {
		FileStream in(filename, ios_base::in);

		vector<const u8*> funcs;
		auto metafuncCount = in.Read<u32>();
		auto metafuncs = in.Read<JitFunc>(metafuncCount);
		for (JitFunc& metafunc : metafuncs) {
			Function func(metafunc.ReturnType);
			u8 inst;
			do {
				inst = in.Read<u8>();
				P::ParseFuncs[inst](in, func, funcs);
			} while (inst != 0x2A);

			funcs.push_back(func.CompileTo(alloc));
		}

		entry = funcs[0];
	}

	int Jitter::Run() {
		return ((int(*)(void))entry)();
	}
}
