#define NO_REG_DEFINE
#include "Assembler.h"
#include <cassert>

using namespace std;

namespace UJit {
	namespace Asm {
		MovR32_I32::MovR32_I32(Reg32 dst, i32 val) : dst(dst), val(val) {}
		size_t MovR32_I32::Size() const { return 5; }
		void MovR32_I32::Compile(u8* target) const { target[0] = 0xB8 + (u8)dst; reinterpret_cast<i32&>(target[1]) = val; }

		PushI8::PushI8(i8 val) : val(val) {}
		size_t PushI8::Size() const { return 2; }
		void PushI8::Compile(u8* target) const { target[0] = 0x6A; target[1] = (u8)val; }

		size_t Ret::Size() const { return 1; }
		void Ret::Compile(u8* target) const { target[0] = 0xC3; }

#ifdef ARCH_X64
		MovR64_R64::MovR64_R64(Reg64 dst, Reg64 src) : dst(dst), src(src) {}
		size_t MovR64_R64::Size() const { return 3; }
		void MovR64_R64::Compile(u8* target) const { target[0] = 0x48; target[1] = 0x89; target[2] = 0xC0 + ((u8)src << 3) + (u8)dst; }

		Pop::Pop(Reg64 dst) : dst(dst) {}
		size_t Pop::Size() const { return 1; }
		void Pop::Compile(u8* target) const { target[0] = 0x58 + (u8)dst; }

		PushR64::PushR64(Reg64 src) : src(src) {}
		size_t PushR64::Size() const { return 1; }
		void PushR64::Compile(u8* target) const { target[0] = 0x50 + (u8)src; }
#endif

		Call::Call(const u8* address) : address(address) {}
		size_t Call::Size() const { return 5; }
		void Call::Compile(u8* target) const { target[0] = 0xE8; reinterpret_cast<u32&>(target[1]) = (u32)(address - &target[5]); }
	}

	void Assembler::Call(const u8* address) { AddInst(new Asm::Call(address)); }
	void Assembler::Mov(Reg32 dst, i32 val) { AddInst(new Asm::MovR32_I32(dst, val)); }
	void Assembler::Push(i8 val) { AddInst(new Asm::PushI8(val)); }
	void Assembler::Ret() { AddInst(new Asm::Ret()); }

#ifdef ARCH_X64
	void Assembler::Mov(Reg64 dst, Reg64 src) { AddInst(new Asm::MovR64_R64(dst, src)); }
	void Assembler::Pop(Reg64 dst) { AddInst(new Asm::Pop(dst)); }
	void Assembler::Push(Reg64 src) { AddInst(new Asm::PushR64(src)); }
	void Assembler::Compile(u8* target) const {
		for (size_t i = 0; i < code.size(); i++) {
			code[i]->Compile(target);
			target += code[i]->Size();
		}
	}
	size_t Assembler::Size() const {
		return size;
	}

	void Assembler::AddInst(Asm::Inst* inst) {
		code.push_back(unique_ptr<Asm::Inst>(inst));
		size += inst->Size();
	}
#endif
}
