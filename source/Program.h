#pragma once
#include "Assembler.h"
#include "Memory.h"
#include <stack>

namespace UJit {
	enum class Type : u32 {
		Void,
		I8, I16, I32, I64
	};

	class Function {
	public:
		Function(Type returnType);
		const u8* CompileTo(MemAllocator& memory);

		void Call(const u8* address);
		void Ldc(intptr_t val);
		void Ret();

	private:
		Assembler assembler;
		Type returnType;
		std::stack<intptr_t> eval;
	};
}
