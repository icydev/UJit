#pragma once
#include "util/Math.h"
#include "ArchDef.h"
#include <vector>
#include <memory>

namespace UJit {
	enum class Reg16 : u8 {
		AX = 0x0,
		SP = 0x4,
		BP = 0x5
	};
	enum class Reg32 : u8 {
		EAX = 0x0,
		ESP = 0x4,
		EBP = 0x5
	};
	enum class Reg64 : u8 {
		RAX = 0x0,
		RSP = 0x4,
		RBP = 0x5
	};

	namespace Asm {
		struct Inst {
			virtual size_t Size() const = 0;
			virtual void Compile(u8* target) const = 0;
		};

		class Call : public Inst {
		public:
			Call(const u8* address);
			virtual size_t Size() const override;
			virtual void Compile(u8* target) const override;

		private:
			const u8* address;
		};

		class MovR32_I32 : public Inst {
		public:
			MovR32_I32(Reg32 dst, i32 val);
			virtual size_t Size() const override;
			virtual void Compile(u8* target) const override;

		private:
			Reg32 dst;
			i32 val;
		};

		class PushI8 : public Inst {
		public:
			PushI8(i8 val);
			virtual size_t Size() const override;
			virtual void Compile(u8* target) const override;

		private:
			i8 val;
		};

		class Ret : public Inst {
		public:
			virtual size_t Size() const override;
			virtual void Compile(u8* target) const override;
		};

#if defined(ARCH_X64)
		class MovR64_R64 : public Inst {
		public:
			MovR64_R64(Reg64 dst, Reg64 src);
			virtual size_t Size() const override;
			virtual void Compile(u8* target) const override;

		private:
			Reg64 dst;
			Reg64 src;
		};

		class Pop : public Inst {
		public:
			Pop(Reg64 dst);
			virtual size_t Size() const override;
			virtual void Compile(u8* target) const override;

		private:
			Reg64 dst;
		};

		class PushR64 : public Inst {
		public:
			PushR64(Reg64 src);
			virtual size_t Size() const override;
			virtual void Compile(u8* target) const override;

		private:
			Reg64 src;
		};
#endif
	}

	class Assembler {
	public:
		void Call(const u8* address);
		void Mov(Reg32 dst, i32 val);
		void Push(i8 val);
		void Ret();

#if defined(ARCH_X64)
		void Mov(Reg64 dst, Reg64 src);
		void Pop(Reg64 reg);
		void Push(Reg64 reg);
#endif

		void Compile(u8* target) const;
		size_t Size() const;

	private:
		void AddInst(Asm::Inst* inst);

		std::vector<std::unique_ptr<const Asm::Inst>> code;
		size_t size = 0;
	};
}

#ifndef NO_REG_DEFINE
#define sp UJit::Reg16::SP
#define bp UJit::Reg16::BP
#define eax UJit::Reg32::EAX
#define esp UJit::Reg32::ESP
#define ebp UJit::Reg32::EBP
#define rax UJit::Reg64::RAX
#define rsp UJit::Reg64::RSP
#define rbp UJit::Reg64::RBP
#endif
