#pragma once
#include <type_traits>

template<typename T>
class Flags {
	using EnumType = typename std::underlying_type<T>::type;

public:
	Flags(T val) : data((EnumType)val) {}

	operator T() const { return (T)data; }

	Flags operator|(Flags rhs) const { return Flags(data | rhs.data); }
	Flags operator&(Flags rhs) const { return Flags(data & rhs.data); }
	Flags operator|(T rhs) const { return Flags(data | (EnumType)rhs); }
	Flags operator&(T rhs) const { return Flags(data & (EnumType)rhs); }

private:
	Flags(EnumType val) : data(val) {}

	EnumType data;
};
