#include "File.h"
#include <fstream>

using namespace std;

namespace util {
	FileStream::FileStream(const std::string &filename, ios_base::openmode mode) : stream(filename, mode) {
		if (!stream) throw errno;
	}

	void FileStream::Seek(size_t pos) {
		stream.seekg(pos, ios::beg);
	}

	void FileStream::ReadInto(void* dest, size_t len) {
		stream.read(reinterpret_cast<char*>(dest), len);
	}

	namespace File {
		FileStream OpenRead(const std::string &filename) {
			return FileStream(filename, ios::in | ios::binary);
		}

		string ReadText(const string &filename) {
			ifstream in(filename, ios::ate | ios::in | ios::binary);
			if (!in) throw errno;

			string contents;
			contents.resize(in.tellg());
			in.seekg(0, ios::beg);
			in.read(&contents[0], contents.size());
			in.close();
			return contents;
		}

		vector<uint8_t> ReadBytes(const string &filename) {
			ifstream in(filename, ios::ate | ios::in | ios::binary);
			if (!in) throw errno;

			vector<uint8_t> contents;
			contents.resize(in.tellg());
			in.seekg(0, ios::beg);
			in.read(reinterpret_cast<char*>(contents.data()), contents.size());
			in.close();
			return contents;
		}

		void WriteText(const std::string &filename, const std::string &text) {
			ofstream out(filename, ios::out | ios::binary | ios::trunc);
			if (!out)
				throw errno;

			out.write(text.c_str(), text.size());
			out.close();
		}

		void AppendText(const std::string &filename, const std::string &text) {
			ofstream out(filename, ios::out | ios::binary | ios::app);
			if (!out)
				throw errno;

			out.write(text.c_str(), text.size());
			out.close();
		}

		void AppendLine(const std::string & filename, const std::string & text) {
			ofstream out(filename, ios::out | ios::binary | ios::app);
			if (!out)
				throw errno;

			out.write(text.c_str(), text.size());
			out.write("\n", 1);
			out.close();
		}
	}
}
