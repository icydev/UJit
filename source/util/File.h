#pragma once
#include <string>
#include <vector>
#include <array>
#include <fstream>

namespace util {
	class FileStream {
	public:
		FileStream(const std::string &filename, std::ios_base::openmode mode);

		void Seek(size_t pos);

		void ReadInto(void* dest, size_t len);

		template<typename T, int Len>
		std::array<T, Len> ReadArray() {
			std::array<T, Len> ret;
			stream.read(reinterpret_cast<char*>(ret.data()), sizeof(T) * Len);
			return ret;
		}

		template<typename T>
		T Read() {
			T ret;
			stream.read(reinterpret_cast<char*>(&ret), sizeof(T));
			return ret;
		}

		template<typename T>
		std::vector<T> Read(size_t count) {
			std::vector<T> ret(count);
			stream.read(reinterpret_cast<char*>(ret.data()), sizeof(T) * count);
			return ret;
		}

	private:
		std::fstream stream;
	};

	namespace File {
		FileStream OpenRead(const std::string &filename);
		std::string ReadText(const std::string &filename);
		std::vector<uint8_t> ReadBytes(const std::string &filename);
		void WriteText(const std::string &filename, const std::string &text);
		void AppendText(const std::string &filename, const std::string &text);
		void AppendLine(const std::string &filename, const std::string &text);
	}
}
