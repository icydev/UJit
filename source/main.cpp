#include "Program.h"
#include "Jitter.h"
#include <iostream>

using namespace std;
using namespace UJit;

int main() {
	Jitter jitter;
	jitter.Compile("../test/test.uib");
	cout << jitter.Run() << endl;
}
