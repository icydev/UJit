#pragma once
#include "util/Math.h"
#include "Program.h"
#include <string>

namespace UJit {
#pragma pack(push, 1)
	struct JitFunc {
		u32 Offset;
		Type ReturnType;
	};
#pragma pack(pop)

	class Jitter {
	public:
		Jitter();
		void Compile(const std::string &filename);
		int Run();

	private:
		const u8* entry;
		MemAllocator alloc;
	};
}
