#pragma once
#include "util/Math.h"
#include "util/Flags.h"
#include <vector>

namespace UJit {
	enum class Permissions {
		Read = 0x1,
		Write = 0x2,
		Execute = 0x4,
		ReadWriteExec = 0x7
	};
	using PermissionFlags = Flags<Permissions>;

	PermissionFlags operator|(Permissions lhs, Permissions rhs);
	PermissionFlags operator&(Permissions lhs, Permissions rhs);

	class MemPage {
	public:
		MemPage(PermissionFlags perms);
		MemPage(MemPage&) = delete;
		MemPage(MemPage&& old);
		~MemPage();

		u8* Ptr();

	private:
		u8* ptr;
	};

	class MemAllocator {
		struct MemBlock {
			u8* Ptr;
			u32 Size;

			MemBlock(u8* ptr, u32 size);
		};

	public:
		MemAllocator(PermissionFlags perms);
		u8* Reserve(u32 size);

	private:
		PermissionFlags perms;
		std::vector<MemPage> pages;
		std::vector<MemBlock> free;

		void addBlock(u8* ptr, u32 size);
	};
}
